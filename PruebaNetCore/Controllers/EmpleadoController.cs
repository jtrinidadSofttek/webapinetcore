﻿using System.Collections.Generic;
using System.Linq;
using Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using PruebaNetCore.ModelsDTO;

namespace PruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        // GET: api/Empleado
        private readonly BusinessEmpleado businessEmpleado;

        public EmpleadoController(BusinessEmpleado _context)
        {
            businessEmpleado = _context;
        }

        // GET: api/Empleado
        [HttpGet]
        public IEnumerable<EmpleadoDto> Get()
        {
            List<EmpleadoDto> empleados = new List<EmpleadoDto>();
            businessEmpleado.GetAll().ToList().ForEach(x => empleados.Add(new EmpleadoDto(x)));
            return empleados ;
        }

        // GET: api/Empleado/5
        [HttpGet("{id}", Name = "GetEmpleado")]
        public ActionResult<EmpleadoDto> Get(int id)
        {
            var empleado = businessEmpleado.GetById(id);
            if (empleado == null)
            {
                return BadRequest();
            }
            else
            {
                var empleadoDto = new EmpleadoDto(empleado);
                return empleadoDto;
            }
        }

        // POST: api/Producto
        [HttpPost]
        public ActionResult<EmpleadoDto> Post([FromBody] EmpleadoDto empleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var empleadoModel = empleado.aEmpleado();
                businessEmpleado.Insert(empleadoModel);
                return CreatedAtAction(nameof(Get), new { id = empleadoModel.Id }, empleadoModel);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // PUT: api/Producto/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] EmpleadoDto empleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var empleadoUpdate = businessEmpleado.GetById(id);
            if (id != empleadoUpdate.Id)
            {
                return BadRequest(id);
            }
            try
            {
                var empleadoModel = empleado.aEmpleado();
                empleadoModel.Id = id;
                businessEmpleado.Update(empleadoModel);
                return NoContent();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var empleado = businessEmpleado.GetById(id);
            if (empleado == null)
            {
                return BadRequest();
            }
            else
            {
                businessEmpleado.Delete(empleado);
                return NoContent();
            }
        }
    }
}