﻿using System.Collections.Generic;
using System.Linq;
using Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using PruebaNetCore.ModelsDTO;

namespace PruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SucursalController : ControllerBase
    {
        private readonly BusinessSucursal businessSucursal;

        public SucursalController(BusinessSucursal _context)
        {
            businessSucursal = _context;
        }

        // GET: api/Sucursal
        [HttpGet]
        public IEnumerable<SucursalDto> Get()
        {
            List<SucursalDto> sucursales = new List<SucursalDto>();
            businessSucursal.GetAll().ToList().ForEach(x => sucursales.Add(new SucursalDto(x)));
            return sucursales ;
        }

        // GET: api/Sucursal/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<SucursalDto> Get(int id)
        {
            var sucursal = businessSucursal.GetById(id);
            if(sucursal == null)
            {
                return BadRequest();
            }
            else
            {
                var sucursalDto = new SucursalDto(sucursal);
                return sucursalDto;
            }
        }

        // POST: api/Sucursal
        [HttpPost]
        public ActionResult<SucursalDto> Post([FromBody] SucursalDto sucursal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var sucursalModel = sucursal.aSucursal();
                businessSucursal.Insert(sucursalModel);
                return CreatedAtAction(nameof(Get), new { id = sucursalModel.Id }, sucursalModel);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }
        
        // PUT: api/Sucursal/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] SucursalDto sucursal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var sucursalUpdate = businessSucursal.GetById(id);
            if (id != sucursalUpdate.Id)
            {
                return BadRequest(id);
            }
            try
            {
                var sucursalModel = sucursal.aSucursal();
                sucursalModel.Id = id;
                businessSucursal.Update(sucursalModel);
                return NoContent();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var sucursal = businessSucursal.GetById(id);
            if(sucursal == null)
            {
                return BadRequest();
            }
            else
            {
                businessSucursal.Delete(sucursal);
                return NoContent();
            }
        }
        
    }
}
