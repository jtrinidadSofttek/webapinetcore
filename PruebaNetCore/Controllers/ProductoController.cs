﻿using System.Collections.Generic;
using System.Linq;
using Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using PruebaNetCore.ModelsDTO;

namespace PruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        // GET: api/Producto
        private readonly BusinessProducto businessProducto;

        public ProductoController(BusinessProducto _context)
        {
            businessProducto = _context;
        }

        // GET: api/Producto
        [HttpGet]
        public IEnumerable<ProductoDto> Get()
        {
            List<ProductoDto> productos = new List<ProductoDto>();
            businessProducto.GetAll().ToList().ForEach(x => productos.Add(new ProductoDto(x)));
            return productos ;
        }

        // GET: api/Producto/5
        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult<ProductoDto> Get(int id)
        {
            var producto = businessProducto.GetById(id);
            if (producto == null)
            {
                return BadRequest();
            }
            else
            {
                ProductoDto productoDto = new ProductoDto(producto);
                return productoDto;
            }
        }

        // POST: api/Producto
        [HttpPost]
        public ActionResult<ProductoDto> Post([FromBody] ProductoDto producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var productoModel = producto.aProducto();
                businessProducto.Insert(productoModel, productoModel.SucursalId);
                return CreatedAtAction(nameof(Get), new { id = productoModel.Id }, productoModel);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // PUT: api/Producto/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProductoDto producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var productoUpdate = businessProducto.GetById(id);
            if (id != productoUpdate.Id)
            {
                return BadRequest(id);
            }
            try
            {
                var productoModel = producto.aProducto();
                productoModel.Id = id;
                businessProducto.Update(productoModel);
                return NoContent();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var producto = businessProducto.GetById(id);
            if (producto == null)
            {
                return BadRequest();
            }
            else
            {
                businessProducto.Delete(producto);
                return NoContent();
            }
        }
    }
}
