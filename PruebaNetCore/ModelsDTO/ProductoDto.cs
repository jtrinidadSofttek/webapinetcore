﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaNetCore.ModelsDTO
{
    public class ProductoDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public int SucursalId { get; set; }

        public ProductoDto() { }

        public ProductoDto(Producto p)
        {
            Id = p.Id;
            Nombre = p.Nombre;
            Cantidad = p.Cantidad;
            SucursalId = p.SucursalId;
        }

        public Producto aProducto()
        {
            Producto p = new Producto();
            p.Id = Id;
            p.Nombre = Nombre;
            p.Cantidad = Cantidad;
            p.SucursalId = SucursalId;
            return p;
        }
    }
}
