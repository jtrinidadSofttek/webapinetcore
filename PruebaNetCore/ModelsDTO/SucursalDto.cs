﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaNetCore.ModelsDTO
{
    public class SucursalDto
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public ICollection<ProductoDto> Productos { get; set; }

        public ICollection<EmpleadoDto> Empleados { get; set; }

        public SucursalDto() { }

        public SucursalDto(Sucursal s)
        {
            Productos = new List<ProductoDto>();
            Empleados = new List<EmpleadoDto>();
            Id = s.Id;
            Nombre = s.Nombre;
            s.Productos.ToList().ForEach(x => Productos.Add(new ProductoDto(x)));
            s.Empleados.ToList().ForEach(x => Empleados.Add(new EmpleadoDto(x)));
        }

        public Sucursal aSucursal()
        {
            Sucursal s = new Sucursal();
            s.Id = Id;
            s.Nombre = Nombre;
            if (Productos != null)
            {
                Productos.ToList().ForEach(x => s.Productos.Add(x.aProducto()));
            }
            else
            {
                s.Productos = new List<Producto>();
            }
            if (Empleados != null)
            {
                Empleados.ToList().ForEach(x => s.Empleados.Add(x.aEmpleado()));
            }
            else
            {
                s.Empleados = new List<Empleado>();
            }

            return s;
        }
    }
}

