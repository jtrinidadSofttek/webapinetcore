﻿using Business;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaNetCore.ModelsDTO
{
    public class EmpleadoDto
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public long Dni { get; set; }

        public int SucursalId { get; set; }

        public EmpleadoDto() { }

        public EmpleadoDto(Empleado e)
        {
            Id = e.Id;
            Nombre = e.Nombre;
            Dni = e.Dni;
            SucursalId = e.SucursalId;
        }

        public Empleado aEmpleado()
        {
            Empleado e = new Empleado();
            e.Id = Id;
            e.Nombre = Nombre;
            e.Dni = Dni;
            e.SucursalId = SucursalId;
            //var s = businessSucursal.GetById(SucursalId);
            //e.Sucursal = s;

            return e;
        }
    }
}
