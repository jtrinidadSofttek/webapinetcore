using AccessData;
using Business;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace PruebaNetCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SucursalContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("myDB")));
            services.AddScoped(typeof(SucursalRepository), typeof(SucursalRepository));
            services.AddScoped(typeof(ProductosRepository), typeof(ProductosRepository));
            services.AddScoped(typeof(EmpleadoRepository), typeof(EmpleadoRepository));
            services.AddTransient<BusinessSucursal>();
            services.AddTransient<BusinessProducto>();
            services.AddTransient<BusinessEmpleado>();
            services.AddControllers();

           /* services.AddControllers().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);*/

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
