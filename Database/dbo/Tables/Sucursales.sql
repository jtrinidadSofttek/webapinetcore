﻿CREATE TABLE [dbo].[Sucursales] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Nombre] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Sucursales] PRIMARY KEY CLUSTERED ([Id] ASC)
);

