﻿CREATE TABLE [dbo].[Empleados] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]     NVARCHAR (MAX) NULL,
    [Dni]        BIGINT         NOT NULL,
    [SucursalId] INT            NOT NULL,
    CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Empleados_Sucursales_SucursalId] FOREIGN KEY ([SucursalId]) REFERENCES [dbo].[Sucursales] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_Empleados_SucursalId]
    ON [dbo].[Empleados]([SucursalId] ASC);

