﻿CREATE TABLE [dbo].[Productos] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]     NVARCHAR (50) NULL,
    [Cantidad]   INT           NOT NULL,
    [SucursalId] INT           NULL,
    CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Productos_Sucursales_SucursalId] FOREIGN KEY ([SucursalId]) REFERENCES [dbo].[Sucursales] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Productos_SucursalId]
    ON [dbo].[Productos]([SucursalId] ASC);

