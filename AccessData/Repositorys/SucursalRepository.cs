﻿using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;
//using AccessData.CodeFirst;

namespace AccessData
{
    public class SucursalRepository :IRepository<Sucursal> 
    {
        private SucursalContext db { get; }

        public SucursalRepository(SucursalContext _db) => this.db = _db;

        public IEnumerable<Sucursal> GetAll() => db.Sucursales.Include(x => x.Productos).Include(x => x.Empleados).ToList();

        public Sucursal GetById(int id) => db.Sucursales.Include(x => x.Productos).Include(x => x.Empleados).FirstOrDefault(x => x.Id == id);

        public void Insert(Sucursal n)
        {
            n.Productos = new List<Producto>();
            n.Empleados = new List<Empleado>();
            db.Sucursales.Add(n);
            db.SaveChanges();
        }

        public void Delete(Sucursal n)
        {
            db.Sucursales.Remove(n);
            db.SaveChanges();
        }

        public void Update(Sucursal n)
        {
            var sucursal = GetById(n.Id);
            sucursal.Nombre = n.Nombre;
            sucursal.Empleados = n.Empleados;
            sucursal.Productos = n.Productos;
            db.SaveChanges();
        }
    }
}
