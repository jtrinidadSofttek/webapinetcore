﻿using Models;
using System.Collections.Generic;
using System.Linq;

namespace AccessData
{
    public class EmpleadoRepository : IRepository<Empleado>
    {
        private SucursalContext db { get; }

        public EmpleadoRepository(SucursalContext _db) => this.db = _db;

        public IEnumerable<Empleado> GetAll() => db.Empleados.ToList();

        public Empleado GetById(int id) => db.Empleados.FirstOrDefault(x => x.Id == id);

        public void Insert(Empleado e)
        {
            db.Empleados.Add(e);
            db.SaveChanges();
        }

        public void Delete(Empleado e)
        {
            db.Empleados.Remove(e);
            db.SaveChanges();
        }

        public void Update(Empleado e)
        {
            var empleado = GetById(e.Id);
            empleado.Nombre = e.Nombre;
            empleado.Dni = e.Dni;
            empleado.Sucursal = e.Sucursal;
            empleado.SucursalId = e.SucursalId;
            db.SaveChanges();
        }
    }
}
