﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessData
{
    public class ProductosRepository : IRepository<Producto>
    {
        private SucursalContext db { get; }

        public ProductosRepository(SucursalContext _db) => this.db = _db;

        public IEnumerable<Producto> GetAll() => db.Productos.ToList();

        public Producto GetById(int id) => db.Productos.FirstOrDefault(x => x.Id == id);

        public void Insert(Producto p)
        {
            db.Productos.Add(p);
            db.SaveChanges();
        }

        public void Insert(Producto p, int idSucursal)
        {
            var sucursal = db.Sucursales.Find(idSucursal);
            if (sucursal.Productos == null)
            {
                sucursal.Productos = new List<Producto>();
            }
            sucursal.Productos.Add(p);
            db.Productos.Add(p);
            db.SaveChanges();
        }

        public void Delete(Producto p)
        {
            db.Productos.Remove(p);
            db.SaveChanges();
        }

        public void Update(Producto p)
        {
            var producto = GetById(p.Id);
            producto.Nombre = p.Nombre;
            producto.Cantidad = p.Cantidad;
            db.SaveChanges();
        }
    }
}

