﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessData
{
    public interface IRepository <TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Insert(TEntity obj);

        void Delete(TEntity obj);

        void Update(TEntity obj);
    }
}
