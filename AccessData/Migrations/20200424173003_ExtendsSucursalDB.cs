﻿//using Microsoft.EntityFrameworkCore.Migrations;

//namespace AccessData.Migrations
//{
//    public partial class ExtendsSucursalDB : Migration
//    {
//        protected override void Up(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.AlterColumn<string>(
//                name: "Nombre",
//                table: "Sucursales",
//                maxLength: 50,
//                nullable: true,
//                oldClrType: typeof(string),
//                oldType: "nvarchar(max)",
//                oldNullable: true);

//            migrationBuilder.AlterColumn<string>(
//                name: "Nombre",
//                table: "Productos",
//                maxLength: 50,
//                nullable: true,
//                oldClrType: typeof(string),
//                oldType: "nvarchar(max)",
//                oldNullable: true);

//            migrationBuilder.CreateTable(
//                name: "Empleados",
//                columns: table => new
//                {
//                    Id = table.Column<int>(nullable: false)
//                        .Annotation("SqlServer:Identity", "1, 1"),
//                    Nombre = table.Column<string>(nullable: true),
//                    Dni = table.Column<long>(nullable: false),
//                    SucursalId = table.Column<int>(nullable: false)
//                },
//                constraints: table =>
//                {
//                    table.PrimaryKey("PK_Empleados", x => x.Id);
//                    table.ForeignKey(
//                        name: "FK_Empleados_Sucursales_SucursalId",
//                        column: x => x.SucursalId,
//                        principalTable: "Sucursales",
//                        principalColumn: "Id",
//                        onDelete: ReferentialAction.Cascade);
//                });

//            migrationBuilder.CreateIndex(
//                name: "IX_Empleados_SucursalId",
//                table: "Empleados",
//                column: "SucursalId");
//        }

//        protected override void Down(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.DropTable(
//                name: "Empleados");

//            migrationBuilder.AlterColumn<string>(
//                name: "Nombre",
//                table: "Sucursales",
//                type: "nvarchar(max)",
//                nullable: true,
//                oldClrType: typeof(string),
//                oldMaxLength: 50,
//                oldNullable: true);

//            migrationBuilder.AlterColumn<string>(
//                name: "Nombre",
//                table: "Productos",
//                type: "nvarchar(max)",
//                nullable: true,
//                oldClrType: typeof(string),
//                oldMaxLength: 50,
//                oldNullable: true);
//        }
//    }
//}
