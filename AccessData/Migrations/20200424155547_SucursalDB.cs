﻿//using Microsoft.EntityFrameworkCore.Migrations;

//namespace AccessData.Migrations
//{
//    public partial class SucursalDB : Migration
//    {
//        protected override void Up(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.CreateTable(
//                name: "Sucursales",
//                columns: table => new
//                {
//                    Id = table.Column<int>(nullable: false)
//                        .Annotation("SqlServer:Identity", "1, 1"),
//                    Nombre = table.Column<string>(nullable: true)
//                },
//                constraints: table =>
//                {
//                    table.PrimaryKey("PK_Sucursales", x => x.Id);
//                });

//            migrationBuilder.CreateTable(
//                name: "Productos",
//                columns: table => new
//                {
//                    Id = table.Column<int>(nullable: false)
//                        .Annotation("SqlServer:Identity", "1, 1"),
//                    Nombre = table.Column<string>(nullable: true),
//                    Cantidad = table.Column<int>(nullable: false),
//                    SucursalId = table.Column<int>(nullable: true)
//                },
//                constraints: table =>
//                {
//                    table.PrimaryKey("PK_Productos", x => x.Id);
//                    table.ForeignKey(
//                        name: "FK_Productos_Sucursales_SucursalId",
//                        column: x => x.SucursalId,
//                        principalTable: "Sucursales",
//                        principalColumn: "Id",
//                        onDelete: ReferentialAction.Restrict);
//                });

//            migrationBuilder.CreateIndex(
//                name: "IX_Productos_SucursalId",
//                table: "Productos",
//                column: "SucursalId");
//        }

//        protected override void Down(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.DropTable(
//                name: "Productos");

//            migrationBuilder.DropTable(
//                name: "Sucursales");
//        }
//    }
//}
