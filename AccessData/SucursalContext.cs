﻿//using AccessData.CodeFirst;
using Microsoft.EntityFrameworkCore;
using Models;

namespace AccessData
{
    public class SucursalContext :DbContext
    {
        public SucursalContext() { }

        public SucursalContext(DbContextOptions<SucursalContext> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empleado>()
                    .HasOne(e => e.Sucursal)
                    .WithMany(s => s.Empleados)
                    .HasForeignKey(e => e.SucursalId);

        }

        //Entidades

        public DbSet<Producto> Productos { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
    }


}
