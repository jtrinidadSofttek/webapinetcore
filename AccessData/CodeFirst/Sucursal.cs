﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AccessData.CodeFirst
{
    public class Sucursal
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        public ICollection<Producto> Productos { get; set; }

        public ICollection<Empleado> Empleados { get; set; }
    }
}
