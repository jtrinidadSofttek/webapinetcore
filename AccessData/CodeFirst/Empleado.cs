﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccessData.CodeFirst
{
    public class Empleado
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public long Dni { get; set; }

        public int SucursalId { get; set; }
        public Sucursal Sucursal { get; set; }
    }
}
