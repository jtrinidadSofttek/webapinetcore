﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AccessData.CodeFirst
{
    public class Producto
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [Range(0, double.MaxValue)]
        public int Cantidad { get; set; }
    }
}
