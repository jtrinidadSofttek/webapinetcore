﻿using AccessData;
//using AccessData.CodeFirst;
using Models;
using System.Collections.Generic;


namespace Business
{
    public class BusinessSucursal :IRepository<Sucursal>
    {
        private SucursalRepository rp { get; }

        public BusinessSucursal(SucursalRepository _rp) => this.rp = _rp;

        public IEnumerable<Sucursal> GetAll() => rp.GetAll();

        public Sucursal GetById(int id) => rp.GetById(id);

        public void Insert(Sucursal n) => rp.Insert(n);

        public void Delete(Sucursal n) => rp.Delete(n);

        public void Update(Sucursal n) => rp.Update(n);
    }
}
