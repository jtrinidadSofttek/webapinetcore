﻿using AccessData;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    public class BusinessEmpleado :IBusiness<Empleado>
    {
        private EmpleadoRepository rp { get; }

        public BusinessEmpleado(EmpleadoRepository _rp) => this.rp = _rp;

        public IEnumerable<Empleado> GetAll() => rp.GetAll();

        public Empleado GetById(int id) => rp.GetById(id);

        public void Insert(Empleado e) => rp.Insert(e);

        public void Delete(Empleado e) => rp.Delete(e);

        public void Update(Empleado e) => rp.Update(e);
    }
}
