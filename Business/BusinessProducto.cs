﻿using AccessData;
using Models;
using System.Collections.Generic;


namespace Business
{
    public class BusinessProducto :IRepository<Producto>
    {
        private ProductosRepository rp { get; }

        public BusinessProducto(ProductosRepository _rp) => this.rp = _rp;

        public IEnumerable<Producto> GetAll() => rp.GetAll();

        public Producto GetById(int id) => rp.GetById(id);

        public void Insert(Producto p, int idSucursal) => rp.Insert(p, idSucursal);

        public void Insert(Producto p) => rp.Insert(p);

        public void Delete(Producto p) => rp.Delete(p);

        public void Update(Producto p) => rp.Update(p);
    }
}
